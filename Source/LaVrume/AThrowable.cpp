// Fill out your copyright notice in the Description page of Project Settings.


#include "AThrowable.h"

// Sets default values
AAThrowable::AAThrowable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAThrowable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAThrowable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SumDeltaTime += DeltaTime;
	
	//f(x) = ax^2+bc+c
	// a = gravity
	// x = deltaTime since beginning
	// b = velocity
}

void AAThrowable::SetStartingAndEndingPoints(FVector start, FVector end)
{
	StartingPoint = start;
	EndingPoint = end;
}
