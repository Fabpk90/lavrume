// Copyright Epic Games, Inc. All Rights Reserved.

#include "LaVrumeGameMode.h"
#include "LaVrumeCharacter.h"
#include "UObject/ConstructorHelpers.h"

ALaVrumeGameMode::ALaVrumeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
