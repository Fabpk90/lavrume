// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AThrowable.generated.h"

UCLASS()
class LAVRUME_API AAThrowable : public AActor
{
	GENERATED_BODY()
private:
	FVector StartingPoint;	
	FVector EndingPoint;
	float SumDeltaTime;
public:	
	// Sets default values for this actor's properties
	AAThrowable();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Velocity;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetStartingAndEndingPoints(FVector start, FVector end);

};
