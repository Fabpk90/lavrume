// Fill out your copyright notice in the Description page of Project Settings.


#include "Sonoscope.h"

#include "Components/TimelineComponent.h"
#include "Engine/PostProcessVolume.h"
#include "Kismet/GameplayStatics.h"
#include "ObjectUtils.h"

// Sets default values for this component's properties
USonoscope::USonoscope()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void USonoscope::BeginPlay()
{
	Super::BeginPlay();

	if(curveWave)
	{
		FOnTimelineFloat OnTimelineUpdate{};
		OnTimelineUpdate.BindUFunction(this, FName("WaveUpdate"));

		FOnTimelineEvent finished{};
		finished.BindUFunction(this, FName("FinishedWave"));

		
		timeline.AddInterpFloat(curveWave, OnTimelineUpdate);
		timeline.SetTimelineFinishedFunc(finished);
	}

	if(waveMaterial)
	{
		waveMaterialDynamic = UMaterialInstanceDynamic::Create(waveMaterial, this);

		//resetting the distance at spawn
		waveMaterialDynamic->SetScalarParameterValue(FName("Distance"), 0.0f);

		TArray<AActor*> Actors;
		UGameplayStatics::GetAllActorsOfClass(this, APostProcessVolume::StaticClass(), Actors);

		if(Actors.Num() > 0)
		{
			APostProcessVolume* PPV = Cast<APostProcessVolume>(Actors[0]);

			FWeightedBlendable WeightedBlendable;
			WeightedBlendable.Object = waveMaterialDynamic;
			WeightedBlendable.Weight = 1.0f;
			
			PPV->Settings.WeightedBlendables.Array.Add(WeightedBlendable);
		}
		else
			print("Make sure you have a post process volume in the scene !");
	}
}


// Called every frame
void USonoscope::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	timeline.TickTimeline(DeltaTime);
}

void USonoscope::OnActivateWave_Implementation()
{
	print("Activating sonoscope");
	timeline.PlayFromStart();
	waveMaterialDynamic->SetVectorParameterValue("WaveLocation", GetOwner()->GetActorLocation());
}

void USonoscope::WaveUpdate(float Val)
{
	waveMaterialDynamic->SetScalarParameterValue(FName("Distance"), Val * waveRange);
}

void USonoscope::FinishedWave()
{
	print("Finished Wave");
	waveMaterialDynamic->SetScalarParameterValue(FName("Distance"), 0.0f);
}