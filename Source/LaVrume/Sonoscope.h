// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Camera/CameraComponent.h"
#include "Components/ActorComponent.h"
#include "Components/TimelineComponent.h"

#include "Sonoscope.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LAVRUME_API USonoscope : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USonoscope();

private:
	FTimeline timeline;

	UCameraComponent* CameraComponent;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category=Wave)
	void OnActivateWave();
	
	void OnActivateWave_Implementation();

	void SetCamera(UCameraComponent* camComp)
	{
		CameraComponent = camComp;
	}

public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Wave)
	class UCurveFloat* curveWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Wave)
	float waveDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Wave)
	float waveRange;

	UPROPERTY(EditAnywhere, Category=Wave)
	UMaterialInstance* waveMaterial;
private:
	UFUNCTION()
	void WaveUpdate(float Val);

	UFUNCTION()
	void FinishedWave();

	UPROPERTY()
	UMaterialInstanceDynamic* waveMaterialDynamic;
};
