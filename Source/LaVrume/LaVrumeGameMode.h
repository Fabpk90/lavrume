// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LaVrumeGameMode.generated.h"

UCLASS(minimalapi)
class ALaVrumeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALaVrumeGameMode();
};



